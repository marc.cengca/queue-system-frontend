import axios from 'axios'

const GET_QUEUE_URL = '/api/getQueue';
const SERVE_NEXT_URL = '/api/serve';
const ENQUEUE_URL = '/api/enqueue';
const DEQUEUE_URL = '/api/dequeue';

export default {
  dequeue(name) {
    return new Promise((resolve, reject) => {

      axios.post(DEQUEUE_URL, {name})
        .then(() => {
          resolve();
        })
        .catch(err => {
          reject(err);
        })
    });

  },
  enqueue(name, mobileNumber) {
    return new Promise((resolve, reject) => {
      const params = {name, mobileNumber};

      axios.post(ENQUEUE_URL, params)
        .then(resp => {
          resolve(resp.data.queueNumber);
        })
        .catch(err => {
          reject(err);
        })
    });
  },
  getQueue() {
    return new Promise((resolve, reject) => {
      axios.get(GET_QUEUE_URL)
        .then(resp => {
          resolve(resp.data);
        })
        .catch(err => {
          reject(err);
        })
    });
  },
  serveNext() {
    return new Promise((resolve, reject) => {
      axios.post(SERVE_NEXT_URL)
        .then(resp => {
          resolve(resp.data);
        })
        .catch(err => {
          reject(err);
        })
    });

  }
}
