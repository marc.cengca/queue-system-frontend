import Vue from 'vue'
import VueRouter from 'vue-router'

import QueuePage from './components/QueuePage.vue'
import ReceptionistPage from './components/ReceptionistPage.vue'
import RegistrationPage from './components/RegistrationPage.vue'

Vue.use(VueRouter);

const routes = [
  {
    path: '',
    redirect: '/registration'
  },
  {
    path: '/registration',
    component: RegistrationPage
  },
  {
    path: '/queue',
    component: QueuePage
  },
  {
    path: '/receptionist',
    component: ReceptionistPage
  }
];

const router = new VueRouter({
  mode: 'history',
  routes
});

export default router;